# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR The Anubis Team
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: bug-anubis@gnu.org\n"
"POT-Creation-Date: 2004-05-27 22:30+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: src/auth.c:31
#, c-format
msgid "Welcome user %s !"
msgstr ""

#: src/auth.c:112
#, c-format
msgid "IDENT: socket() failed: %s."
msgstr ""

#: src/auth.c:124
#, c-format
msgid "IDENT: connect() failed: %s."
msgstr ""

#: src/auth.c:130
#, c-format
msgid "IDENT: connected to %s:%u"
msgstr ""

#: src/auth.c:138
#, c-format
msgid "IDENT: stream_write() failed: %s."
msgstr ""

#: src/auth.c:145
#, c-format
msgid "IDENT: recvline() failed: %s."
msgstr ""

#: src/auth.c:155
msgid "IDENT: incorrect data."
msgstr ""

#: src/auth.c:165
msgid "IDENT: data probably encrypted with DES..."
msgstr ""

#: src/auth.c:172
msgid "IDENT: incorrect data (DES deciphered)."
msgstr ""

#: src/auth.c:188
#, c-format
msgid "IDENT: resolved remote user to %s."
msgstr ""

#: src/authmode.c:396
msgid "Database not specified"
msgstr ""

#: src/authmode.c:403
#, c-format
msgid "Cannot open database %s: %s"
msgstr ""

#: src/authmode.c:412
#, c-format
msgid "Found record for %s"
msgstr ""

#: src/authmode.c:417
#, c-format
msgid "Cannot retrieve data from the SASL database: %s"
msgstr ""

#: src/authmode.c:422
#, c-format
msgid "Record for %s not found"
msgstr ""

#: src/authmode.c:465 src/daemon.c:323 src/transmode.c:66
msgid "The MTA has not been specified. Set the REMOTE-MTA or LOCAL-MTA."
msgstr ""

#: src/authmode.c:491 src/map.c:155 src/net.c:137 src/net.c:198
#: src/transmode.c:92
#, c-format
msgid "Illegal address length received for host %s"
msgstr ""

#: src/authmode.c:502 src/transmode.c:103
msgid "Loop not allowed. Connection rejected."
msgstr ""

#: src/authmode.c:532 src/transmode.c:142
msgid "Connection terminated."
msgstr ""

#: src/authmode.c:534 src/transmode.c:144
msgid "Connection closed successfully."
msgstr ""

#: src/authmode.c:539 src/transmode.c:149
msgid "PAM: Session closed."
msgstr ""

#: src/authmode.c:542 src/transmode.c:152
msgid "PAM: failed to release authenticator."
msgstr ""

#: src/daemon.c:48
#, c-format
msgid "daemon() failed. %s."
msgstr ""

#: src/daemon.c:55
#, c-format
msgid "Can't fork. %s."
msgstr ""

#: src/daemon.c:63
msgid "setsid() failed."
msgstr ""

#: src/daemon.c:75
#, c-format
msgid "%s daemon startup succeeded."
msgstr ""

#: src/daemon.c:91
#, c-format
msgid "Child [%lu] finished. Exit status: %s. %d client left."
msgid_plural "Child [%lu] finished. Exit status: %s. %d clients left."
msgstr[0] ""
msgstr[1] ""

#: src/daemon.c:95
msgid "OK"
msgstr ""

#: src/daemon.c:95
msgid "ERROR"
msgstr ""

#: src/daemon.c:137
msgid "WARNING: An unprivileged user has not been specified!"
msgstr ""

#: src/daemon.c:186
msgid "GNU Anubis is running..."
msgstr ""

#: src/daemon.c:196 src/exec.c:114
#, c-format
msgid "accept() failed: %s."
msgstr ""

#: src/daemon.c:214
#, c-format
msgid "TCP wrappers: connection from %s:%u rejected."
msgstr ""

#: src/daemon.c:232
#, c-format
msgid "Too many clients. Connection from %s:%u rejected."
msgstr ""

#: src/daemon.c:238
#, c-format
msgid "Connection from %s:%u"
msgstr ""

#: src/daemon.c:244
#, c-format
msgid "daemon: Can't fork. %s."
msgstr ""

#: src/env.c:163
#, c-format
msgid "Try '%s --help' for more information."
msgstr ""

#: src/env.c:265
msgid "PAM: Session opened (restrictions applied)."
msgstr ""

#: src/env.c:267
msgid "PAM: Not authenticated to use GNU Anubis."
msgstr ""

#: src/env.c:277 src/main.c:152
#, c-format
msgid "UID:%d, GID:%d, EUID:%d, EGID:%d"
msgstr ""

#: src/env.c:308
#, c-format
msgid "Invalid user ID: %s"
msgstr ""

#: src/env.c:313
#, c-format
msgid "Invalid user name: %s"
msgstr ""

#: src/env.c:335
#, c-format
msgid "Wrong permissions on %s. Set 0600."
msgstr ""

#: src/env.c:359
#, c-format
msgid "%s is not a regular file or a symbolic link."
msgstr ""

#: src/env.c:380
#, c-format
msgid "Unknown mode: %s"
msgstr ""

#: src/errs.c:82
#, c-format
msgid "Couldn't write to socket: %s."
msgstr ""

#: src/errs.c:93
#, c-format
msgid "Unknown host %s."
msgstr ""

#: src/errs.c:96
#, c-format
msgid "%s: host name is valid but does not have an IP address."
msgstr ""

#: src/errs.c:99
#, c-format
msgid "%s: unrecoverable name server error occured."
msgstr ""

#: src/errs.c:102
#, c-format
msgid "%s: a temporary name server error occured. Try again later."
msgstr ""

#: src/errs.c:104
#, c-format
msgid "%s: unknown DNS error %d."
msgstr ""

#: src/esmtp.c:57
msgid "Using the ESMTP CRAM-MD5 authentication..."
msgstr ""

#: src/esmtp.c:63 src/esmtp.c:102
msgid "Server rejected the AUTH command."
msgstr ""

#: src/esmtp.c:71 src/esmtp.c:110 src/esmtp.c:121
#, c-format
msgid "Challenge decoded: %s"
msgstr ""

#: src/esmtp.c:90 src/esmtp.c:131
#, c-format
msgid "ESMTP AUTH: %s."
msgstr ""

#: src/esmtp.c:96
msgid "Using the ESMTP LOGIN authentication..."
msgstr ""

#: src/exec.c:81
msgid "#1 socket() failed."
msgstr ""

#: src/exec.c:85
msgid "#2 socket() failed."
msgstr ""

#: src/exec.c:94
#, c-format
msgid "#1 bind() failed: %s."
msgstr ""

#: src/exec.c:98
#, c-format
msgid "#2 bind() failed: %s."
msgstr ""

#: src/exec.c:102 src/net.c:216
#, c-format
msgid "listen() failed: %s."
msgstr ""

#: src/exec.c:106
#, c-format
msgid "getsockname() failed: %s."
msgstr ""

#: src/exec.c:110
#, c-format
msgid "connect() failed: %s."
msgstr ""

#: src/exec.c:120
#, c-format
msgid "socketpair() failed: %s."
msgstr ""

#: src/exec.c:134
#, c-format
msgid "Local program [%lu] finished."
msgstr ""

#: src/exec.c:170
#, c-format
msgid "Executing %s %s..."
msgstr ""

#: src/exec.c:177
msgid "fork() failed."
msgstr ""

#: src/exec.c:191
#, c-format
msgid "execvp() failed: %s"
msgstr ""

#: src/gpg.c:50
#, c-format
msgid "GPGME: %s."
msgstr ""

#: src/gpg.c:73
#, c-format
msgid "Install GPGME version %s or later."
msgstr ""

#: src/gpg.c:78
#, c-format
msgid "GPGME: failed. %s."
msgstr ""

#: src/gpg.c:122 src/gpg.c:282
#, c-format
msgid "GPGME: Cannot list keys: %s"
msgstr ""

#: src/guile.c:79
#, c-format
msgid "cannot open guile output file %s: %s"
msgstr ""

#: src/guile.c:258
msgid "missing procedure name"
msgstr ""

#: src/guile.c:273
#, c-format
msgid "%s not a procedure object"
msgstr ""

#: src/guile.c:309
#, c-format
msgid "Bad car type in return from %s"
msgstr ""

#: src/guile.c:324
#, c-format
msgid "Bad cdr type in return from %s"
msgstr ""

#: src/guile.c:328
#, c-format
msgid "Bad return type from %s"
msgstr ""

#: src/help.c:82
msgid ""
"\n"
"GNU Anubis is free software; you can redistribute it and/or modify\n"
"it under the terms of the GNU General Public License as published by\n"
"the Free Software Foundation; either version 2 of the License, or\n"
"(at your option) any later version."
msgstr ""

#: src/help.c:86
msgid ""
"\n"
"GNU Anubis is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"GNU General Public License for more details."
msgstr ""

#: src/help.c:90
msgid ""
"\n"
"You should have received a copy of the GNU General Public License\n"
"along with GNU Anubis; if not, write to the Free Software\n"
"Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA"
msgstr ""

#: src/help.c:93
msgid ""
"\n"
"GNU Anubis is released under the GPL with the additional exemption that\n"
"compiling, linking, and/or using OpenSSL is allowed.\n"
msgstr ""

#: src/help.c:101
msgid "Usage: anubis [options]\n"
msgstr ""

#: src/help.c:102
msgid ""
" -b, --bind [HOST:]PORT       Specify the TCP port on which GNU Anubis listens\n"
"                              for connections. The default HOST is INADDR_ANY,\n"
"                              and default PORT is 24 (private mail system)."
msgstr ""

#: src/help.c:105
msgid ""
" -r, --remote-mta HOST[:PORT] Specify a remote SMTP host name or IP address.\n"
"                              The default PORT number is 25."
msgstr ""

#: src/help.c:107
msgid ""
" -l, --local-mta FILE         Execute a local SMTP server, which works on\n"
"                              standard input and output (inetd-type program).\n"
"                              This option excludes the '--remote-mta' option."
msgstr ""

#: src/help.c:110
msgid " -m, --mode=MODE              Select operation mode."
msgstr ""

#: src/help.c:111
msgid "                              MODE is either \"transparent\" or \"auth\""
msgstr ""

#: src/help.c:112
msgid " -f, --foreground             Foreground mode."
msgstr ""

#: src/help.c:113
msgid ""
" -i, --stdio                  Use the SMTP protocol (OMP/Tunnel) as described\n"
"                              in RFC 821 on standard input and output."
msgstr ""

#: src/help.c:115
msgid "Output options:\n"
msgstr ""

#: src/help.c:116
msgid " -s, --silent                 Work silently."
msgstr ""

#: src/help.c:117
msgid " -v, --verbose                Work noisily."
msgstr ""

#: src/help.c:118
msgid " -D, --debug                  Debug mode."
msgstr ""

#: src/help.c:119
msgid ""
"\n"
"Miscellaneous options:\n"
msgstr ""

#: src/help.c:120
msgid " -c, --check-config           Run the configuration file syntax checker."
msgstr ""

#: src/help.c:121
msgid ""
" --show-config-options        Print a list of configuration options used\n"
"                              to build GNU Anubis."
msgstr ""

#: src/help.c:123
msgid " --relax-perm-check           Do not check a user config file permissions."
msgstr ""

#: src/help.c:124
msgid " --altrc FILE                 Specify alternate system configuration file."
msgstr ""

#: src/help.c:125
msgid " --norc                       Ignore system configuration file."
msgstr ""

#: src/help.c:126
msgid " --version                    Print version number and copyright."
msgstr ""

#: src/help.c:127
msgid " --help                       It's obvious..."
msgstr ""

#: src/help.c:128
#, c-format
msgid ""
"\n"
"Report bugs to <%s>.\n"
msgstr ""

#: src/map.c:68
#, c-format
msgid "%s remapped to %s@localhost."
msgstr ""

#: src/map.c:121
msgid "Translation map: incorrect syntax."
msgstr ""

#: src/mem.c:37
msgid "malloc() failed. Cannot allocate enough memory."
msgstr ""

#: src/mem.c:52
msgid "realloc() failed. Cannot reallocate enough memory."
msgstr ""

#: src/misc.c:308
msgid "Can't find out my own hostname"
msgstr ""

#: src/net.c:45
msgid "SERVER"
msgstr ""

#: src/net.c:48
msgid "CLIENT"
msgstr ""

#: src/net.c:122
msgid "Getting remote host information..."
msgstr ""

#: src/net.c:152 src/net.c:184
msgid "Can't create stream socket."
msgstr ""

#: src/net.c:155
#, c-format
msgid "Connecting to %s:%u..."
msgstr ""

#: src/net.c:157
#, c-format
msgid "Couldn't connect to %s:%u. %s."
msgstr ""

#: src/net.c:162
#, c-format
msgid "Connected to %s:%u"
msgstr ""

#: src/net.c:212
#, c-format
msgid "bind() failed: %s."
msgstr ""

#: src/net.c:213
#, c-format
msgid "GNU Anubis bound to %s:%u"
msgstr ""

#: src/net.c:242
msgid "Short write"
msgstr ""

#: src/socks.c:53
msgid "Using SOCKS Proxy..."
msgstr ""

#: src/socks.c:71
#, c-format
msgid "SOCKS proxy: %s"
msgstr ""

#: src/socks.c:136
msgid "Address must be an IP, not a domain name."
msgstr ""

#: src/socks.c:171 src/socks.c:379
msgid "SOCKS Proxy Connection: succeeded."
msgstr ""

#: src/socks.c:174
msgid "Request rejected or failed."
msgstr ""

#: src/socks.c:177
msgid "Request rejected."
msgstr ""

#: src/socks.c:180
msgid "Request rejected, because the client program and identd reported different User-IDs."
msgstr ""

#: src/socks.c:184 src/socks.c:406
msgid "Server reply is not valid."
msgstr ""

#: src/socks.c:222
msgid "Possibly not a SOCKS proxy service."
msgstr ""

#: src/socks.c:234
msgid "SOCKS Proxy AUTH method: NO AUTHENTICATION REQUIRED"
msgstr ""

#: src/socks.c:238
msgid "SOCKS Proxy AUTH method: USER NAME/PASSWORD"
msgstr ""

#: src/socks.c:241
msgid "Cannot send null user name or password."
msgstr ""

#: src/socks.c:281
msgid "Bad user name or password."
msgstr ""

#: src/socks.c:285
msgid "SOCKS Proxy AUTH: succeeded."
msgstr ""

#: src/socks.c:288
msgid "Server does not accept any method."
msgstr ""

#: src/socks.c:291
msgid "Server does not accept an AUTH method."
msgstr ""

#: src/socks.c:382
msgid "General SOCKS server failure."
msgstr ""

#: src/socks.c:385
msgid "Connection not allowed by a ruleset."
msgstr ""

#: src/socks.c:388
msgid "Network unreachable."
msgstr ""

#: src/socks.c:391
msgid "Host unreachable."
msgstr ""

#: src/socks.c:394
msgid "Connection refused."
msgstr ""

#: src/socks.c:397
msgid "TTL expired."
msgstr ""

#: src/socks.c:400
msgid "Command not supported."
msgstr ""

#: src/socks.c:403
msgid "Address type not supported."
msgstr ""

#: src/quit.c:31
msgid "Signal Caught. Exiting Cleanly..."
msgstr ""

#: src/quit.c:38
msgid "Timeout! Exiting..."
msgstr ""

#: src/rcfile.c:121
#, c-format
msgid "cannot stat file `%s': %s"
msgstr ""

#: src/rcfile.c:131
#, c-format
msgid "File `%s' has already been read"
msgstr ""

#: src/rcfile.c:177
#, c-format
msgid "Reading system configuration file %s..."
msgstr ""

#: src/rcfile.c:187
#, c-format
msgid "Reading user configuration file %s..."
msgstr ""

#: src/rcfile.c:679
#, c-format
msgid "No such section: %s"
msgstr ""

#: src/rcfile.l:180
#, c-format
msgid "Stray character in config: \\%03o. Possibly missing quotes around the string"
msgstr ""

#: src/rcfile.l:380
#, c-format
msgid "Anubis RC file error: %s."
msgstr ""

#: src/rcfile.y:162 src/rcfile.y:170
#, c-format
msgid "Section %s already defined"
msgstr ""

#: src/rcfile.y:217
#, c-format
msgid "unknown keyword: %s"
msgstr ""

#: src/rcfile.y:510
msgid "missing replacement value"
msgstr ""

#: src/rcfile.y:1122
msgid "Unknown regexp modifier"
msgstr ""

#: src/rcfile.y:1213
msgid "STOP"
msgstr ""

#: src/rcfile.y:1218
#, c-format
msgid "Calling %s"
msgstr ""

#: src/rcfile.y:1224
#, c-format
msgid "ADD %s [%s] %s"
msgstr ""

#: src/rcfile.y:1234
#, c-format
msgid "MODIFY %s [%s] [%s] %s"
msgstr ""

#: src/rcfile.y:1247
#, c-format
msgid "REMOVE HEADER [%s]"
msgstr ""

#: src/rcfile.y:1270
#, c-format
msgid "Executing %s"
msgstr ""

#: src/rcfile.y:1341
#, c-format
msgid "Matched trigger \"%s\""
msgstr ""

#: src/rcfile.y:1345
#, c-format
msgid "Matched condition %s[%s] \"%s\""
msgstr ""

#: src/rcfile.y:1475
#, c-format
msgid "Unknown section: %s"
msgstr ""

#: src/rcfile.y:1517
msgid "program is not allowed in this section"
msgstr ""

#: src/regex.c:109
#, c-format
msgid "INTERNAL ERROR at %s:%d: missing or invalid regex"
msgstr ""

#: src/regex.c:306
#, c-format
msgid "regcomp() failed at %s: %s."
msgstr ""

#: src/regex.c:377
#, c-format
msgid "pcre_compile() failed at offset %d: %s."
msgstr ""

#: src/regex.c:402
#, c-format
msgid "pcre_fullinfo() failed: %d."
msgstr ""

#: src/regex.c:414
msgid "Matched, but too many substrings."
msgstr ""

#: src/regex.c:425
#, c-format
msgid "Get substring %d failed (%d)."
msgstr ""

#: src/ssl.c:57
msgid "Seeding random number generator..."
msgstr ""

#: src/ssl.c:61
msgid "Unable to seed random number generator."
msgstr ""

#: src/ssl.c:76
#, c-format
msgid "Write error: %s"
msgstr ""

#: src/ssl.c:93
#, c-format
msgid "Read error: %s"
msgstr ""

#: src/ssl.c:275
msgid "SSLv23_client_method() failed."
msgstr ""

#: src/ssl.c:279 src/ssl.c:357
msgid "Can't create SSL_CTX object."
msgstr ""

#: src/ssl.c:284 src/ssl.c:373
msgid "SSL_CTX_set_cipher_list() failed."
msgstr ""

#: src/ssl.c:305 src/tls.c:161
msgid "Initializing the TLS/SSL connection with MTA..."
msgstr ""

#: src/ssl.c:308 src/ssl.c:393
msgid "Can't create a new SSL structure for a connection."
msgstr ""

#: src/ssl.c:323 src/tls.c:193
#, c-format
msgid "TLS/SSL handshake failed: %s"
msgstr ""

#: src/ssl.c:353
msgid "SSLv23_server_method() failed."
msgstr ""

#: src/ssl.c:361
msgid "SSL_CTX_use_certificate_file() failed."
msgstr ""

#: src/ssl.c:365
msgid "SSL_CTX_use_PrivateKey_file() failed."
msgstr ""

#: src/ssl.c:369
msgid "Private key does not match the certificate public key."
msgstr ""

#: src/ssl.c:390 src/tls.c:244
msgid "Initializing the TLS/SSL connection with MUA..."
msgstr ""

#: src/ssl.c:405 src/tls.c:275
msgid "TLS/SSL handshake failed!"
msgstr ""

#: src/ssl.c:439
#, c-format
msgid "%s connection using %s (%u bit)"
msgid_plural "%s connection using %s (%u bits)"
msgstr[0] ""
msgstr[1] ""

#: src/ssl.c:451
#, c-format
msgid "Server public key is %d bit"
msgid_plural "Server public key is %d bits"
msgstr[0] ""
msgstr[1] ""

#: src/ssl.c:457
msgid "Certificate:"
msgstr ""

#: src/ssl.c:459
msgid "X509_NAME_oneline [subject] failed!"
msgstr ""

#: src/ssl.c:462
#, c-format
msgid "Subject: %s"
msgstr ""

#: src/ssl.c:464
msgid "X509_NAME_oneline [issuer] failed!"
msgstr ""

#: src/ssl.c:467
#, c-format
msgid "Issuer:  %s"
msgstr ""

#: src/tls.c:176 src/tls.c:253
#, c-format
msgid "TLS Error reading `%s': %s"
msgstr ""

#: src/tls.c:296
msgid "No certificate was sent."
msgstr ""

#: src/tls.c:300
msgid "The certificate is not trusted."
msgstr ""

#: src/tls.c:304
msgid "The certificate has expired."
msgstr ""

#: src/tls.c:308
msgid "The certificate is not yet activated."
msgstr ""

#: src/tls.c:316
msgid "No certificate was found!"
msgstr ""

#: src/tls.c:320
msgid "The certificate is trusted."
msgstr ""

#: src/tls.c:351
#, c-format
msgid "Anonymous DH using prime of %d bit."
msgid_plural "Anonymous DH using prime of %d bits."
msgstr[0] ""
msgstr[1] ""

#: src/tls.c:361
#, c-format
msgid "Ephemeral DH using prime of %d bit."
msgid_plural "Ephemeral DH using prime of %d bits."
msgstr[0] ""
msgstr[1] ""

#: src/tls.c:373
#, c-format
msgid "- Protocol: %s\n"
msgstr ""

#: src/tls.c:376
#, c-format
msgid "- Certificate Type: %s\n"
msgstr ""

#: src/tls.c:379
#, c-format
msgid "- Compression: %s\n"
msgstr ""

#: src/tls.c:382
#, c-format
msgid "- Cipher: %s\n"
msgstr ""

#: src/tls.c:385
#, c-format
msgid "- MAC: %s\n"
msgstr ""

#: src/tls.c:412
#, c-format
msgid " - Certificate info:\n"
msgstr ""

#: src/tls.c:416
#, c-format
msgid " - Certificate is valid since: %s"
msgstr ""

#: src/tls.c:418
#, c-format
msgid " - Certificate expires: %s"
msgstr ""

#: src/tls.c:424
#, c-format
msgid " - Certificate fingerprint: "
msgstr ""

#: src/tls.c:434
#, c-format
msgid " - Certificate serial number: "
msgstr ""

#: src/tls.c:443
#, c-format
msgid "Certificate public key: "
msgstr ""

#: src/tls.c:445
#, c-format
msgid "RSA\n"
msgstr ""

#: src/tls.c:446
#, c-format
msgid " Modulus: %d bit\n"
msgid_plural " Modulus: %d bits\n"
msgstr[0] ""
msgstr[1] ""

#: src/tls.c:451
#, c-format
msgid "DSA\n"
msgstr ""

#: src/tls.c:452
#, c-format
msgid " Exponent: %d bit\n"
msgid_plural " Exponent: %d bits\n"
msgstr[0] ""
msgstr[1] ""

#: src/tls.c:457
#, c-format
msgid "UNKNOWN\n"
msgstr ""

#: src/tls.c:459
#, c-format
msgid " - Certificate version: #%d\n"
msgstr ""

#: src/tls.c:466
#, c-format
msgid " - Certificate Issuer's DN: %s\n"
msgstr ""

#: src/tunnel.c:288 src/tunnel.c:351
msgid "Transferring message(s)..."
msgstr ""

#: src/tunnel.c:349
msgid "Starting SMTP session..."
msgstr ""

#: src/tunnel.c:429
msgid "Using the TLS/SSL encryption..."
msgstr ""

#: src/tunnel.c:441 src/tunnel.c:548
#, c-format
msgid "WARNING: %s"
msgstr ""

#: src/tunnel.c:442
msgid "STARTTLS command failed."
msgstr ""

#: src/tunnel.c:540
msgid "Using the 'ONEWAY' TLS/SSL encryption..."
msgstr ""

#: src/tunnel.c:550
msgid "STARTTLS (ONEWAY) command failed."
msgstr ""

#: src/tunnel.c:577
#, c-format
msgid "getpeername() failed: %s."
msgstr ""

#: lib/getopt.c:629 lib/getopt.c:641
#, c-format
msgid "%s: option `%s' is ambiguous\n"
msgstr ""

#: lib/getopt.c:674 lib/getopt.c:678
#, c-format
msgid "%s: option `--%s' doesn't allow an argument\n"
msgstr ""

#: lib/getopt.c:687 lib/getopt.c:692
#, c-format
msgid "%s: option `%c%s' doesn't allow an argument\n"
msgstr ""

#: lib/getopt.c:728 lib/getopt.c:741 lib/getopt.c:1030 lib/getopt.c:1043
#, c-format
msgid "%s: option `%s' requires an argument\n"
msgstr ""

#: lib/getopt.c:779 lib/getopt.c:782
#, c-format
msgid "%s: unrecognized option `--%s'\n"
msgstr ""

#: lib/getopt.c:790 lib/getopt.c:793
#, c-format
msgid "%s: unrecognized option `%c%s'\n"
msgstr ""

#: lib/getopt.c:840 lib/getopt.c:843
#, c-format
msgid "%s: illegal option -- %c\n"
msgstr ""

#: lib/getopt.c:849 lib/getopt.c:852
#, c-format
msgid "%s: invalid option -- %c\n"
msgstr ""

#: lib/getopt.c:899 lib/getopt.c:910 lib/getopt.c:1096 lib/getopt.c:1109
#, c-format
msgid "%s: option requires an argument -- %c\n"
msgstr ""

#: lib/getopt.c:962 lib/getopt.c:973
#, c-format
msgid "%s: option `-W %s' is ambiguous\n"
msgstr ""

#: lib/getopt.c:997 lib/getopt.c:1009
#, c-format
msgid "%s: option `-W %s' doesn't allow an argument\n"
msgstr ""

#: lib/obstack.c:403 lib/obstack.c:406
msgid "memory exhausted"
msgstr ""
