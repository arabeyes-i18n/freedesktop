# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-03-31 16:22+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: lilylib.py:60
msgid "lilylib module"
msgstr ""

#: lilylib.py:63 lilypond-book.py:84 lilypond.py:127 midi2ly.py:100
#: mup2ly.py:75 old-lilypond-book.py:129 main.cc:140
msgid "print this help"
msgstr ""

#: lilylib.py:110 midi2ly.py:136 mup2ly.py:130
#, python-format
msgid "Copyright (c) %s by"
msgstr ""

#: lilylib.py:114 midi2ly.py:141 mup2ly.py:135
msgid "Distributed under terms of the GNU General Public License."
msgstr ""

#: lilylib.py:116 midi2ly.py:142 mup2ly.py:136
msgid "It comes with NO WARRANTY."
msgstr ""

#: lilylib.py:123 warn.cc:25
#, c-format, python-format
msgid "warning: %s"
msgstr ""

#: lilylib.py:126 warn.cc:31
#, c-format, python-format
msgid "error: %s"
msgstr ""

#: lilylib.py:130
#, python-format
msgid "Exiting (%d)..."
msgstr ""

#: lilylib.py:190 midi2ly.py:223 mup2ly.py:219
#, python-format
msgid "Usage: %s [OPTIONS]... FILE"
msgstr ""

#: lilylib.py:194 midi2ly.py:227 mup2ly.py:223 main.cc:205
#, c-format
msgid "Options:"
msgstr ""

#: lilylib.py:198 midi2ly.py:231 mup2ly.py:227 main.cc:209
#, c-format, python-format
msgid "Report bugs to %s."
msgstr ""

#: lilylib.py:218
#, python-format
msgid "Binary %s has version %s, looking for version %s"
msgstr ""

#: lilylib.py:252
#, python-format
msgid "Opening pipe `%s'"
msgstr ""

#: lilylib.py:267 lilypond-book.py:767
#, python-format
msgid "`%s' failed (%d)"
msgstr ""

#: lilylib.py:272 lilylib.py:331 lilypond-book.py:768 lilypond.py:530
#: old-lilypond-book.py:228
msgid "The error log is as follows:"
msgstr ""

#: lilylib.py:303 midi2ly.py:259 mup2ly.py:255
#, python-format
msgid "Invoking `%s'"
msgstr ""

#: lilylib.py:305
#, python-format
msgid "Running %s..."
msgstr ""

#: lilylib.py:324
#, python-format
msgid "`%s' failed (%s)"
msgstr ""

#: lilylib.py:327 midi2ly.py:265 mup2ly.py:263
msgid "(ignored)"
msgstr ""

#: lilylib.py:345 midi2ly.py:275 mup2ly.py:273
#, python-format
msgid "Cleaning %s..."
msgstr ""

#: lilylib.py:509
msgid "Removing output file"
msgstr ""

#: lilypond-book.py:69
msgid ""
"Process LilyPond snippets in hybrid HTML, LaTeX or texinfo document.\n"
"Example usage:\n"
"\n"
"   lilypond-book --filter=\"tr '[a-z]' '[A-Z]'\" BOOK\n"
"   lilypond-book --filter=\"convert-ly --no-version --from=2.0.0 -\" BOOK\n"
"   lilypond-book --process='lilypond-bin -I include' BOOK\n"
"\n"
msgstr ""

#. Bug in option parser: --output=foe is taken as an abbreviation
#. for --output-format.
#: lilypond-book.py:82 old-lilypond-book.py:123 main.cc:139
msgid "EXT"
msgstr ""

#: lilypond-book.py:82 old-lilypond-book.py:123
msgid "use output format EXT (texi [default], texi-html, latex, html)"
msgstr ""

#: lilypond-book.py:83
msgid "FILTER"
msgstr ""

#: lilypond-book.py:83
msgid "pipe snippets through FILTER [convert-ly -n -]"
msgstr ""

#: lilypond-book.py:85 lilypond-book.py:87 lilypond.py:131
#: old-lilypond-book.py:130 main.cc:142 main.cc:147
msgid "DIR"
msgstr ""

#: lilypond-book.py:85
msgid "add DIR to include path"
msgstr ""

#: lilypond-book.py:86
msgid "COMMAND"
msgstr ""

#: lilypond-book.py:86
msgid "process ly_files using COMMAND FILE..."
msgstr ""

#: lilypond-book.py:87
msgid "write output to DIR"
msgstr ""

#: lilypond-book.py:88 lilypond.py:149 midi2ly.py:105 mup2ly.py:78
#: old-lilypond-book.py:140 main.cc:150
msgid "be verbose"
msgstr ""

#: lilypond-book.py:89 old-lilypond-book.py:141
msgid "print version information"
msgstr ""

#: lilypond-book.py:90 lilypond.py:151 midi2ly.py:107 mup2ly.py:80
#: old-lilypond-book.py:142 main.cc:151
msgid "show warranty and copyright"
msgstr ""

#: lilypond-book.py:373
#, python-format
msgid "deprecated ly-option used: %s"
msgstr ""

#: lilypond-book.py:374
#, python-format
msgid "compatibility mode translation: %s"
msgstr ""

#: lilypond-book.py:394
#, python-format
msgid "ignoring unknown ly option: %s"
msgstr ""

#: lilypond-book.py:448
#, python-format
msgid "file not found: %s"
msgstr ""

#: lilypond-book.py:750
#, python-format
msgid "Opening filter `%s'"
msgstr ""

#: lilypond-book.py:862
#, python-format
msgid "cannot determine format for: %s"
msgstr ""

#: lilypond-book.py:903
msgid "Output would overwrite input file; use --output."
msgstr ""

#: lilypond-book.py:910
#, python-format
msgid "Reading %s..."
msgstr ""

#: lilypond-book.py:924
msgid "Dissecting..."
msgstr ""

#: lilypond-book.py:952
msgid "Writing snippets..."
msgstr ""

#: lilypond-book.py:957
msgid "Processing..."
msgstr ""

#: lilypond-book.py:960
msgid "All snippets are up to date..."
msgstr ""

#: lilypond-book.py:963
#, python-format
msgid "Compiling %s..."
msgstr ""

#: lilypond-book.py:971
#, python-format
msgid "Processing include: %s"
msgstr ""

#: lilypond-book.py:987 lilypond.py:693 midi2ly.py:1017
#: old-lilypond-book.py:1570
#, python-format
msgid "getopt says: `%s'"
msgstr ""

#. # FIXME
#. # do -P or -p by default?
#. #help_summary = _ ("Run LilyPond using LaTeX for titling")
#: lilypond.py:120
msgid "Run LilyPond, add titles, generate printable document."
msgstr ""

#: lilypond.py:126
msgid "write Makefile dependencies for every input file"
msgstr ""

#: lilypond.py:128
msgid "print even more output"
msgstr ""

#: lilypond.py:129 lilypond.py:136 midi2ly.py:102 old-lilypond-book.py:136
#: old-lilypond-book.py:137 main.cc:143 main.cc:146
msgid "FILE"
msgstr ""

#: lilypond.py:129
msgid "find pfa fonts used in FILE"
msgstr ""

#: lilypond.py:130
msgid "make HTML file with links to all output"
msgstr ""

#: lilypond.py:131
msgid "add DIR to LilyPond's search path"
msgstr ""

#: lilypond.py:133
#, python-format
msgid "keep all output, output to directory %s.dir"
msgstr ""

#: lilypond.py:134
msgid "don't run LilyPond"
msgstr ""

#: lilypond.py:135 main.cc:145
msgid "produce MIDI output only"
msgstr ""

#: lilypond.py:136 midi2ly.py:102 mup2ly.py:76 main.cc:146
msgid "write output to FILE"
msgstr ""

#: lilypond.py:137 old-lilypond-book.py:138
msgid "RES"
msgstr ""

#: lilypond.py:138 old-lilypond-book.py:139
msgid "set the resolution of the preview to RES"
msgstr ""

#: lilypond.py:139
msgid "do not generate PDF output"
msgstr ""

#: lilypond.py:140
msgid "do not generate PostScript output"
msgstr ""

#: lilypond.py:141
msgid "generate PDF output"
msgstr ""

#: lilypond.py:142
msgid "generate PostScript output"
msgstr ""

#: lilypond.py:143
msgid "use pdflatex to generate PDF output"
msgstr ""

#: lilypond.py:144
msgid "generate PNG page images"
msgstr ""

#: lilypond.py:145
msgid "make a picture of the first system"
msgstr ""

#: lilypond.py:146
msgid "generate PS.GZ"
msgstr ""

#: lilypond.py:147
msgid "run in safe-mode"
msgstr ""

#: lilypond.py:148
msgid "KEY=VAL"
msgstr ""

#: lilypond.py:148
msgid "change global setting KEY to VAL"
msgstr ""

#: lilypond.py:150 midi2ly.py:106 mup2ly.py:79 main.cc:149
msgid "print version number"
msgstr ""

#: lilypond.py:234
#, python-format
msgid "no such setting: `%s'"
msgstr ""

#: lilypond.py:284
#, python-format
msgid "LilyPond crashed (signal %d)."
msgstr ""

#: lilypond.py:285
msgid "Please submit a bug report to bug-lilypond@gnu.org"
msgstr ""

#: lilypond.py:291
#, python-format
msgid "LilyPond failed on input file %s (exit status %d)"
msgstr ""

#: lilypond.py:294
#, python-format
msgid "LilyPond failed on an input file (exit status %d)"
msgstr ""

#: lilypond.py:295
msgid "Continuing..."
msgstr ""

#: lilypond.py:306
#, python-format
msgid "Analyzing %s..."
msgstr ""

#: lilypond.py:364
#, python-format
msgid "no LilyPond output found for `%s'"
msgstr ""

#: lilypond.py:431
#, python-format
msgid "invalid value: `%s'"
msgstr ""

#: lilypond.py:529
msgid "LaTeX failed on the output file."
msgstr ""

#: lilypond.py:586
msgid ""
"Trying create PDF, but no PFA fonts found.\n"
"Using bitmap fonts instead. This will look bad."
msgstr ""

#. no ps header?
#: lilypond.py:635
#, python-format
msgid "not a PostScript file: `%s'"
msgstr ""

#: lilypond.py:680
#, python-format
msgid "Writing HTML menu `%s'"
msgstr ""

#: lilypond.py:800
msgid "pseudo filter"
msgstr ""

#: lilypond.py:803
msgid "pseudo filter only for single input file"
msgstr ""

#: lilypond.py:808 old-lilypond-book.py:1643
msgid "no files specified on command line"
msgstr ""

#: lilypond.py:840
#, python-format
msgid "filename should not contain spaces: `%s'"
msgstr ""

#: lilypond.py:880
msgid "Running LilyPond failed. Rerun with --verbose for a trace."
msgstr ""

#: lilypond.py:921
msgid "Failed to make PS file. Rerun with --verbose for a trace."
msgstr ""

#: lilypond.py:951
msgid "Running LaTeX failed. Rerun with --verbose for a trace."
msgstr ""

#: lilypond.py:963 input-file-results.cc:74
#, c-format, python-format
msgid "dependencies output to `%s'..."
msgstr ""

#: lilypond.py:974
#, python-format
msgid "%s output to <stdout>..."
msgstr ""

#: lilypond.py:979 includable-lexer.cc:57 input-file-results.cc:217
#: input-file-results.cc:224 lily-guile.cc:86
#, c-format, python-format
msgid "can't find file: `%s'"
msgstr ""

#: lilypond.py:1002
#, python-format
msgid "%s output to %s..."
msgstr ""

#: lilypond.py:1005
#, python-format
msgid "can't find file: `%s.%s'"
msgstr ""

#. temp_dir = os.path.join (original_dir,  '%s.dir' % program_name)
#. original_dir = os.getcwd ()
#. keep_temp_dir_p = 0
#: midi2ly.py:94
msgid "Convert MIDI to LilyPond source."
msgstr ""

#: midi2ly.py:97
msgid "print absolute pitches"
msgstr ""

#: midi2ly.py:98 midi2ly.py:103
msgid "DUR"
msgstr ""

#: midi2ly.py:98
msgid "quantise note durations on DUR"
msgstr ""

#: midi2ly.py:99
msgid "print explicit durations"
msgstr ""

#: midi2ly.py:101
msgid "ALT[:MINOR]"
msgstr ""

#: midi2ly.py:101
msgid "set key: ALT=+sharps|-flats; MINOR=1"
msgstr ""

#: midi2ly.py:103
msgid "quantise note starts on DUR"
msgstr ""

#: midi2ly.py:104
msgid "DUR*NUM/DEN"
msgstr ""

#: midi2ly.py:104
msgid "allow tuplet durations DUR*NUM/DEN"
msgstr ""

#: midi2ly.py:108
msgid "treat every text as a lyric"
msgstr ""

#: midi2ly.py:149 mup2ly.py:143 input.cc:88
msgid "warning: "
msgstr ""

#: midi2ly.py:164 midi2ly.py:1017 midi2ly.py:1082 mup2ly.py:146 mup2ly.py:160
#: input.cc:93
msgid "error: "
msgstr ""

#: midi2ly.py:165 mup2ly.py:161
msgid "Exiting ... "
msgstr ""

#: midi2ly.py:263 mup2ly.py:260
#, python-format
msgid "command exited with value %d"
msgstr ""

#: midi2ly.py:1001
#, python-format
msgid "%s output to `%s'..."
msgstr ""

#: midi2ly.py:1032
msgid "Example:"
msgstr ""

#: midi2ly.py:1082
msgid "no files specified on command line."
msgstr ""

#: mup2ly.py:70
msgid "Convert mup to LilyPond source."
msgstr ""

#: mup2ly.py:73
msgid "debug"
msgstr ""

#: mup2ly.py:74
msgid "define macro NAME [optional expansion EXP]"
msgstr ""

#: mup2ly.py:77
msgid "only pre-process"
msgstr ""

#: mup2ly.py:1075
#, python-format
msgid "no such context: %s"
msgstr ""

#: mup2ly.py:1299
#, python-format
msgid "Processing `%s'..."
msgstr ""

#: mup2ly.py:1318
#, python-format
msgid "Writing `%s'..."
msgstr ""

#. # FIXME
#. # do -P or -p by default?
#. #help_summary = _ ("Run LilyPond using LaTeX for titling")
#: old-lilypond-book.py:118
msgid "Process LilyPond snippets in hybrid HTML, LaTeX or texinfo document"
msgstr ""

#: old-lilypond-book.py:124 old-lilypond-book.py:125 old-lilypond-book.py:127
#: old-lilypond-book.py:128
msgid "DIM"
msgstr ""

#: old-lilypond-book.py:124
msgid "default fontsize for music.  DIM is assumed to be in points"
msgstr ""

#: old-lilypond-book.py:125
msgid "deprecated, use --default-music-fontsize"
msgstr ""

#: old-lilypond-book.py:126
msgid "OPT"
msgstr ""

#: old-lilypond-book.py:126
msgid "pass OPT quoted to the lilypond command line"
msgstr ""

#: old-lilypond-book.py:127
msgid "force fontsize for all inline lilypond. DIM is assumed to be in points"
msgstr ""

#: old-lilypond-book.py:128
msgid "deprecated, use --force-music-fontsize"
msgstr ""

#: old-lilypond-book.py:130
msgid "include path"
msgstr ""

#: old-lilypond-book.py:131
msgid "write dependencies"
msgstr ""

#: old-lilypond-book.py:132
msgid "PREF"
msgstr ""

#: old-lilypond-book.py:132
msgid "prepend PREF before each -M dependency"
msgstr ""

#: old-lilypond-book.py:133
msgid "don't run lilypond"
msgstr ""

#: old-lilypond-book.py:134
msgid "don't generate pictures"
msgstr ""

#: old-lilypond-book.py:135
msgid "strip all lilypond blocks from output"
msgstr ""

#: old-lilypond-book.py:136
msgid "filename main output file"
msgstr ""

#: old-lilypond-book.py:137
msgid "where to place generated files"
msgstr ""

#: old-lilypond-book.py:227
msgid "LaTeX failed."
msgstr ""

#: getopt-long.cc:146
#, c-format
msgid "option `%s' requires an argument"
msgstr ""

#: getopt-long.cc:150
#, c-format
msgid "option `%s' doesn't allow an argument"
msgstr ""

#: getopt-long.cc:154
#, c-format
msgid "unrecognized option: `%s'"
msgstr ""

#: getopt-long.cc:161
#, c-format
msgid "invalid argument `%s' to option `%s'"
msgstr ""

#: warn.cc:44
#, c-format
msgid "programming error: %s"
msgstr ""

#: warn.cc:45
msgid "Continuing; crossing fingers"
msgstr ""

#: accidental.cc:219 key-signature-interface.cc:139
#, c-format
msgid "accidental `%s' not found"
msgstr ""

#: accidental-engraver.cc:167
#, c-format
msgid "Accidental typesetting list must begin with context-name: %s"
msgstr ""

#: accidental-engraver.cc:194
#, c-format
msgid "ignoring unknown accidental: %s"
msgstr ""

#: accidental-engraver.cc:211
#, c-format
msgid "Accidental rule must be pair or context-name; Found %s"
msgstr ""

#: afm.cc:145
#, c-format
msgid "Error parsing AFM file: `%s'"
msgstr ""

#: all-font-metrics.cc:100
#, c-format
msgid "checksum mismatch for font file: `%s'"
msgstr ""

#: all-font-metrics.cc:102
#, c-format
msgid "does not match: `%s'"
msgstr ""

#: all-font-metrics.cc:107
msgid "Rebuild all .afm files, and remove all .pk and .tfm files."
msgstr ""

#: all-font-metrics.cc:109
msgid "Rerun with -V to show font paths."
msgstr ""

#: all-font-metrics.cc:111
msgid "A script for removing font-files is delivered with the source-code:"
msgstr ""

#: all-font-metrics.cc:192
#, c-format
msgid "can't find font: `%s'"
msgstr ""

#: all-font-metrics.cc:193
msgid "Loading default font"
msgstr ""

#: all-font-metrics.cc:208
#, c-format
msgid "can't find default font: `%s'"
msgstr ""

#: all-font-metrics.cc:209 includable-lexer.cc:59 input-file-results.cc:218
#, c-format
msgid "(search path: `%s')"
msgstr ""

#: all-font-metrics.cc:210
msgid "Giving up"
msgstr ""

#: auto-change-iterator.cc:62 change-iterator.cc:61
msgid "Can't switch translators, I'm there already"
msgstr ""

#: bar-check-iterator.cc:68
#, c-format
msgid "barcheck failed at: %s"
msgstr ""

#: beam.cc:151
msgid "beam has less than two visible stems"
msgstr ""

#: beam.cc:156
msgid "removing beam with less than two stems"
msgstr ""

#: beam.cc:1040
msgid "no viable initial configuration found: may not find good beam slope"
msgstr ""

#: beam-engraver.cc:139
msgid "already have a beam"
msgstr ""

#: beam-engraver.cc:212
msgid "unterminated beam"
msgstr ""

#: beam-engraver.cc:245 chord-tremolo-engraver.cc:179
msgid "stem must have Rhythmic structure"
msgstr ""

#: beam-engraver.cc:259
msgid "stem doesn't fit in beam"
msgstr ""

#: beam-engraver.cc:260
msgid "beam was started here"
msgstr ""

#: break-align-interface.cc:214
#, c-format
msgid "No spacing entry from %s to `%s'"
msgstr ""

#: change-iterator.cc:22
#, c-format
msgid "can't change `%s' to `%s'"
msgstr ""

#.
#. We could change the current translator's id, but that would make
#. errors hard to catch
#.
#. last->translator_id_string_  = get_change ()->change_to_id_string_;
#.
#: change-iterator.cc:87
msgid "I'm one myself"
msgstr ""

#: change-iterator.cc:90
msgid "none of these in my family"
msgstr ""

#: chord-tremolo-engraver.cc:100
#, c-format
msgid "Chord tremolo with %d elements. Must have two elements."
msgstr ""

#: chord-tremolo-engraver.cc:141
msgid "unterminated chord tremolo"
msgstr ""

#: chord-tremolo-iterator.cc:64
msgid "no one to print a tremolos"
msgstr ""

#: clef.cc:64
#, c-format
msgid "clef `%s' not found"
msgstr ""

#: cluster.cc:123
#, c-format
msgid "unknown cluster style `%s'"
msgstr ""

#: coherent-ligature-engraver.cc:84
#, c-format
msgid "gotcha: ptr=%ul"
msgstr ""

#: coherent-ligature-engraver.cc:96
#, c-format
msgid "distance=%f"
msgstr ""

#: coherent-ligature-engraver.cc:139
#, c-format
msgid "Coherent_ligature_engraver: setting `spacing-increment = 0.01': ptr=%ul"
msgstr ""

#: context.cc:180
#, c-format
msgid "Cannot find or create `%s' called `%s'"
msgstr ""

#: context.cc:217
#, c-format
msgid "can't find or create: `%s'"
msgstr ""

#: context-def.cc:115
#, c-format
msgid "Program has no such type: `%s'"
msgstr ""

#: custos.cc:85
#, c-format
msgid "custos `%s' not found"
msgstr ""

#: dimensions.cc:13
msgid "NaN"
msgstr ""

#: dynamic-engraver.cc:183 span-dynamic-performer.cc:86
msgid "can't find start of (de)crescendo"
msgstr ""

#: dynamic-engraver.cc:193
msgid "already have a decrescendo"
msgstr ""

#: dynamic-engraver.cc:195
msgid "already have a crescendo"
msgstr ""

#: dynamic-engraver.cc:198
msgid "Cresc started here"
msgstr ""

#: dynamic-engraver.cc:305
msgid "unterminated (de)crescendo"
msgstr ""

#: event.cc:49
#, c-format
msgid "Transposition by %s makes alteration larger than two"
msgstr ""

#: event.cc:72
#, c-format
msgid "octave check failed; expected %s, found: %s"
msgstr ""

#: event-chord-iterator.cc:56 output-property-music-iterator.cc:29
#, c-format
msgid "Junking event: `%s'"
msgstr ""

#: extender-engraver.cc:143 extender-engraver.cc:153
msgid "unterminated extender"
msgstr ""

#: folded-repeat-iterator.cc:65
msgid "no one to print a repeat brace"
msgstr ""

#: glissando-engraver.cc:102
msgid "Unterminated glissando."
msgstr ""

#: global-context.cc:151
#, c-format
msgid "can't find `%s' context"
msgstr ""

#: gourlay-breaking.cc:195
#, c-format
msgid "Optimal demerits: %f"
msgstr ""

#: gourlay-breaking.cc:200
msgid "No feasible line breaking found"
msgstr ""

#: gregorian-ligature-engraver.cc:59
#, c-format
msgid "\\%s ignored"
msgstr ""

#: gregorian-ligature-engraver.cc:64
#, c-format
msgid "implied \\%s added"
msgstr ""

#: hairpin.cc:98
msgid "decrescendo too small"
msgstr ""

#: hairpin.cc:99
msgid "crescendo too small"
msgstr ""

#: horizontal-bracket-engraver.cc:57
msgid "Don't have that many brackets."
msgstr ""

#: horizontal-bracket-engraver.cc:66
msgid "Conflicting note group events."
msgstr ""

#: hyphen-engraver.cc:96
msgid "removing unterminated hyphen"
msgstr ""

#: hyphen-engraver.cc:110
msgid "unterminated hyphen; removing"
msgstr ""

#: includable-lexer.cc:50
msgid "include files are not allowed"
msgstr ""

#: input.cc:99
msgid "non fatal error: "
msgstr ""

#: input.cc:107 source-file.cc:147 source-file.cc:240
msgid "position unknown"
msgstr ""

#: input-file-results.cc:78 source-file.cc:55
#, c-format
msgid "can't open file: `%s'"
msgstr ""

#: input-file-results.cc:142
#, c-format
msgid "Now processing `%s'"
msgstr ""

#: key-performer.cc:90
msgid "FIXME: key change merge"
msgstr ""

#: kpath.cc:75
#, c-format
msgid "kpathsea can not find TFM file: `%s'"
msgstr ""

#: ligature-engraver.cc:152
msgid "can't find start of ligature"
msgstr ""

#: ligature-engraver.cc:158
msgid "no right bound"
msgstr ""

#: ligature-engraver.cc:184
msgid "already have a ligature"
msgstr ""

#: ligature-engraver.cc:200
msgid "no left bound"
msgstr ""

#: ligature-engraver.cc:255
msgid "unterminated ligature"
msgstr ""

#: ligature-engraver.cc:279
msgid "ignoring rest: ligature may not contain rest"
msgstr ""

#: ligature-engraver.cc:280
msgid "ligature was started here"
msgstr ""

#: lily-guile.cc:88
#, c-format
msgid "(load path: `%s')"
msgstr ""

#: lily-guile.cc:584
#, c-format
msgid "Can't find property type-check for `%s' (%s)."
msgstr ""

#: lily-guile.cc:587
msgid "Perhaps you made a typing error?"
msgstr ""

#: lily-guile.cc:593
msgid "Doing assignment anyway."
msgstr ""

#: lily-guile.cc:607
#, c-format
msgid "Type check for `%s' failed; value `%s' must be of type `%s'"
msgstr ""

#: lookup.cc:169
msgid "round filled box horizontal extent smaller than blot; decreasing blot"
msgstr ""

#: lookup.cc:174
msgid "round filled box vertical extent smaller than blot; decreasing blot"
msgstr ""

#: main.cc:100
msgid ""
"This program is free software.  It is covered by the GNU General Public\n"
"License and you are welcome to change it and/or distribute copies of it\n"
"under certain conditions.  Invoke as `lilypond-bin --warranty' for more\n"
"information.\n"
msgstr ""

#: main.cc:106
msgid ""
"    This program is free software; you can redistribute it and/or\n"
"modify it under the terms of the GNU General Public License version 2\n"
"as published by the Free Software Foundation.\n"
"\n"
"    This program is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\n"
"General Public License for more details.\n"
"\n"
"    You should have received a copy (refer to the file COPYING) of the\n"
"GNU General Public License along with this program; if not, write to\n"
"the Free Software Foundation, Inc., 59 Temple Place - Suite 330,\n"
"Boston, MA 02111-1307, USA.\n"
msgstr ""

#: main.cc:135
msgid "EXPR"
msgstr ""

#: main.cc:136
msgid "set options, use -e '(ly-option-usage)' for help"
msgstr ""

#: main.cc:139
msgid "use output format EXT"
msgstr ""

#: main.cc:141
msgid "FIELD"
msgstr ""

#: main.cc:141
msgid "write header field to BASENAME.FIELD"
msgstr ""

#: main.cc:142
msgid "add DIR to search path"
msgstr ""

#: main.cc:143
msgid "use FILE as init file"
msgstr ""

#: main.cc:144
msgid "write Makefile dependencies"
msgstr ""

#: main.cc:147
msgid "prepend DIR to dependencies"
msgstr ""

#: main.cc:148
msgid "run in safe mode"
msgstr ""

#: main.cc:171
#, c-format
msgid ""
"Copyright (c) %s by\n"
"%s  and others."
msgstr ""

#. No version number or newline here.  It confuses help2man.
#: main.cc:197
#, c-format
msgid "Usage: %s [OPTIONS]... FILE..."
msgstr ""

#: main.cc:199
#, c-format
msgid "Typeset music and/or produce MIDI from FILE."
msgstr ""

#: main.cc:201
#, c-format
msgid "LilyPond produces beautiful music notation."
msgstr ""

#: main.cc:203
#, c-format
msgid "For more information, see %s"
msgstr ""

#: main.cc:369
#, c-format
msgid "This option is for developers only."
msgstr ""

#: main.cc:370
#, c-format
msgid "Read the sources for more information."
msgstr ""

#: mensural-ligature.cc:183
msgid "Mensural_ligature:unexpected case fall-through"
msgstr ""

#: mensural-ligature.cc:193
msgid "Mensural_ligature: (join_left == 0)"
msgstr ""

#: mensural-ligature-engraver.cc:248 mensural-ligature-engraver.cc:383
msgid "unexpected case fall-through"
msgstr ""

#: mensural-ligature-engraver.cc:259
msgid "ligature with less than 2 heads -> skipping"
msgstr ""

#: mensural-ligature-engraver.cc:279
msgid "can not determine pitch of ligature primitive -> skipping"
msgstr ""

#: mensural-ligature-engraver.cc:302
msgid "prime interval within ligature -> skipping"
msgstr ""

#: mensural-ligature-engraver.cc:312
msgid "mensural ligature: duration none of L, B, S -> skipping"
msgstr ""

#: midi-item.cc:153
#, c-format
msgid "no such MIDI instrument: `%s'"
msgstr ""

#: midi-item.cc:257
msgid "silly pitch"
msgstr ""

#: midi-item.cc:273
#, c-format
msgid "Experimental: temporarily fine tuning (of %d cents) a channel."
msgstr ""

#: midi-stream.cc:40
#, c-format
msgid "could not write file: `%s'"
msgstr ""

#: my-lily-lexer.cc:185
#, c-format
msgid "Identifier name is a keyword: `%s'"
msgstr ""

#: my-lily-lexer.cc:207
#, c-format
msgid "error at EOF: %s"
msgstr ""

#: my-lily-parser.cc:45
msgid "Parsing..."
msgstr ""

#: my-lily-parser.cc:57
msgid "Braces don't match"
msgstr ""

#.
#. music for the softenon children?
#.
#: new-fingering-engraver.cc:155
msgid "music for the martians."
msgstr ""

#: new-fingering-engraver.cc:235
msgid "Fingerings are also not down?! Putting them down anyway."
msgstr ""

#: new-lyric-combine-music-iterator.cc:172
#, c-format
msgid "cannot find Voice: %s"
msgstr ""

#: note-collision.cc:384
msgid "Too many clashing notecolumns.  Ignoring them."
msgstr ""

#: note-head.cc:139
#, c-format
msgid "note head `%s' not found"
msgstr ""

#: paper-def.cc:73
#, c-format
msgid "paper output to `%s'..."
msgstr ""

#: paper-score.cc:72
#, c-format
msgid "Element count %d (spanners %d) "
msgstr ""

#: paper-score.cc:76
msgid "Preprocessing graphical objects..."
msgstr ""

#: parse-scm.cc:81
msgid "GUILE signaled an error for the expression beginning here"
msgstr ""

#: percent-repeat-engraver.cc:110
msgid "Don't know how to handle a percent repeat of this length."
msgstr ""

#: percent-repeat-engraver.cc:164
msgid "unterminated percent repeat"
msgstr ""

#: percent-repeat-iterator.cc:53
msgid "no one to print a percent"
msgstr ""

#: performance.cc:51
msgid "Track ... "
msgstr ""

#: performance.cc:94
msgid "Creator: "
msgstr ""

#: performance.cc:114
msgid "at "
msgstr ""

#: performance.cc:172
#, c-format
msgid "MIDI output to `%s'..."
msgstr ""

#: phrasing-slur-engraver.cc:105
msgid "unterminated phrasing slur"
msgstr ""

#: phrasing-slur-engraver.cc:123
msgid "can't find start of phrasing slur"
msgstr ""

#: piano-pedal-engraver.cc:238
msgid "Need 3 strings for piano pedals. No pedal made. "
msgstr ""

#: piano-pedal-engraver.cc:253 piano-pedal-engraver.cc:268
#: piano-pedal-performer.cc:82
#, c-format
msgid "can't find start of piano pedal: `%s'"
msgstr ""

#: piano-pedal-engraver.cc:321
#, c-format
msgid "can't find start of piano pedal bracket: `%s'"
msgstr ""

#: property-iterator.cc:94
#, c-format
msgid "Not a grob name, `%s'."
msgstr ""

#: quote-iterator.cc:181
#, c-format
msgid "In quotation: junking event %s"
msgstr ""

#: relative-octave-check.cc:25
msgid "Failed octave check, got: "
msgstr ""

#: rest.cc:137
#, c-format
msgid "rest `%s' not found"
msgstr ""

#: rest-collision.cc:145
msgid "rest direction not set.  Cannot resolve collision."
msgstr ""

#: rest-collision.cc:193
msgid "too many colliding rests"
msgstr ""

#: scm-option.cc:52
#, c-format
msgid "lilypond -e EXPR means:"
msgstr ""

#: scm-option.cc:54
#, c-format
msgid "  Evalute the Scheme EXPR before parsing any .ly files."
msgstr ""

#: scm-option.cc:56
#, c-format
msgid ""
"  Multiple -e options may be given, they will be evaluated sequentially."
msgstr ""

#: scm-option.cc:58
#, c-format
msgid ""
"  The function ly-set-option allows for access to some internal variables."
msgstr ""

#: scm-option.cc:60
#, c-format
msgid "Usage: lilypond-bin -e \"(ly-set-option SYMBOL VAL)\""
msgstr ""

#: scm-option.cc:62
#, c-format
msgid "Where SYMBOL VAL pair is any of:"
msgstr ""

#: scm-option.cc:143 scm-option.cc:176
msgid "Unknown internal option!"
msgstr ""

#: score.cc:125
msgid "Interpreting music... "
msgstr ""

#: score.cc:135
msgid "Need music in a score"
msgstr ""

#: score.cc:145
#, c-format
msgid "elapsed time: %.2f seconds"
msgstr ""

#: score-engraver.cc:103
#, c-format
msgid "can't find `%s'"
msgstr ""

#: score-engraver.cc:104
msgid "Fonts have not been installed properly.  Aborting"
msgstr ""

#: score-engraver.cc:189
#, c-format
msgid "unbound spanner `%s'"
msgstr ""

#: script-engraver.cc:96
msgid "Don't know how to interpret articulation:"
msgstr ""

#: script-engraver.cc:97
msgid "Scheme encoding: "
msgstr ""

#. this shouldn't happen, but let's continue anyway.
#: separation-item.cc:53 separation-item.cc:97
msgid "Separation_item:  I've been drinking too much"
msgstr ""

#: simple-spacer.cc:248
#, c-format
msgid "No spring between column %d and next one"
msgstr ""

#: slur-engraver.cc:121
msgid "unterminated slur"
msgstr ""

#. How to shut up this warning, when Voice_devnull_engraver has
#. eaten start event?
#: slur-engraver.cc:139
msgid "can't find start of slur"
msgstr ""

#: source-file.cc:68
#, c-format
msgid "Huh?  Got %d, expected %d characters"
msgstr ""

#: spacing-spanner.cc:385
#, c-format
msgid "Global shortest duration is %s"
msgstr ""

#: spring-smob.cc:32
#, c-format
msgid "#<spring smob d= %f>"
msgstr ""

#: stem.cc:119
msgid "Weird stem size; check for narrow beams"
msgstr ""

#: stem.cc:648
#, c-format
msgid "flag `%s' not found"
msgstr ""

#: stem.cc:661
#, c-format
msgid "flag stroke `%s' not found"
msgstr ""

#: stem-engraver.cc:97
msgid "tremolo duration is too long"
msgstr ""

#: stem-engraver.cc:128
#, c-format
msgid "Adding note head to incompatible stem (type = %d)"
msgstr ""

#: stem-engraver.cc:129
msgid "Don't you want polyphonic voices instead?"
msgstr ""

#: system.cc:181
#, c-format
msgid "Element count %d."
msgstr ""

#: system.cc:335
#, c-format
msgid "Grob count %d"
msgstr ""

#: system.cc:349
msgid "Calculating line breaks..."
msgstr ""

#: text-spanner-engraver.cc:65
msgid "can't find start of text spanner"
msgstr ""

#: text-spanner-engraver.cc:79
msgid "already have a text spanner"
msgstr ""

#: text-spanner-engraver.cc:143
msgid "unterminated text spanner"
msgstr ""

#: tfm.cc:83
#, c-format
msgid "can't find ascii character: %d"
msgstr ""

#. Not using ngettext's plural feature here, as this message is
#. more of a programming error.
#: tfm-reader.cc:108
#, c-format
msgid "TFM header of `%s' has only %u word (s)"
msgstr ""

#: tfm-reader.cc:142
#, c-format
msgid "%s: TFM file has %u parameters, which is more than the %u I can handle"
msgstr ""

#: tie-engraver.cc:164
msgid "lonely tie"
msgstr ""

#: time-scaled-music-iterator.cc:24
msgid "no one to print a tuplet start bracket"
msgstr ""

#. If there is no such symbol, we default to the numbered style.
#. (Here really with a warning!)
#: time-signature.cc:95
#, c-format
msgid "time signature symbol `%s' not found; reverting to numbered style"
msgstr ""

#.
#. Todo: should make typecheck?
#.
#. OTOH, Tristan Keuris writes 8/20 in his Intermezzi.
#.
#: time-signature-engraver.cc:57
#, c-format
msgid "Found strange time signature %d/%d."
msgstr ""

#: translator-ctors.cc:53
#, c-format
msgid "unknown translator: `%s'"
msgstr ""

#: translator-group.cc:108
#, c-format
msgid "can't find: `%s'"
msgstr ""

#: tuplet-bracket.cc:448
msgid "Killing tuplet bracket across linebreak."
msgstr ""

#: vaticana-ligature.cc:92
msgid "ascending vaticana style flexa"
msgstr ""

#: vaticana-ligature.cc:181
msgid "Vaticana_ligature: zero join (delta_pitch == 0)"
msgstr ""

#: vaticana-ligature-engraver.cc:342
#, c-format
msgid ""
"ignored prefix (es) `%s' of this head according to restrictions of the "
"selected ligature style"
msgstr ""

#: vaticana-ligature-engraver.cc:572
#, c-format
msgid "Vaticana_ligature_engraver: setting `spacing-increment = %f': ptr=%ul"
msgstr ""

#: volta-engraver.cc:140
msgid "No volta spanner to end"
msgstr ""

#: volta-engraver.cc:151
msgid "Already have a volta spanner.  Stopping that one prematurely."
msgstr ""

#: volta-engraver.cc:155
msgid "Also have a stopped spanner.  Giving up."
msgstr ""

#: parser.yy:114
msgid "Tag must be symbol or list of symbols."
msgstr ""

#: parser.yy:513
msgid "Identifier should have alphabetic characters only"
msgstr ""

#: parser.yy:776
msgid "More alternatives than repeats.  Junking excess alternatives."
msgstr ""

#: parser.yy:857 parser.yy:864
msgid "\\applycontext takes function argument"
msgstr ""

#: parser.yy:1014
msgid "\\apply takes function argument"
msgstr ""

#: parser.yy:1377
msgid "Can't find music"
msgstr ""

#: parser.yy:1495
msgid "Second argument must be pitch list."
msgstr ""

#: parser.yy:1532 parser.yy:1537 parser.yy:2070
msgid "Have to be in Lyric mode for lyrics"
msgstr ""

#: parser.yy:1622
msgid "Expecting string as script definition"
msgstr ""

#: parser.yy:1829 parser.yy:1884
#, c-format
msgid "not a duration: %d"
msgstr ""

#: parser.yy:1980
msgid "Have to be in Note mode for notes"
msgstr ""

#: parser.yy:2085
msgid "Have to be in Chord mode for chords"
msgstr ""

#: parser.yy:2232
msgid "need integer number arg"
msgstr ""

#: parser.yy:2383
msgid "Suspect duration found following this beam"
msgstr ""

#: lexer.ll:184
#, c-format
msgid "input renamed to: `%s'"
msgstr ""

#: lexer.ll:210
msgid "EOF found inside a comment"
msgstr ""

#: lexer.ll:225
msgid "\\maininput not allowed outside init files"
msgstr ""

#: lexer.ll:249
#, c-format
msgid "wrong or undefined identifier: `%s'"
msgstr ""

#. backup rule
#: lexer.ll:258
msgid "Missing end quote"
msgstr ""

#: lexer.ll:400
msgid "Brace found at end of lyric.  Did you forget a space?"
msgstr ""

#: lexer.ll:501
msgid "Brace found at end of markup.  Did you forget a space?"
msgstr ""

#: lexer.ll:584
#, c-format
msgid "invalid character: `%c'"
msgstr ""

#: lexer.ll:656 lexer.ll:657
#, c-format
msgid "unknown escaped string: `\\%s'"
msgstr ""

#: lexer.ll:754 lexer.ll:755
#, c-format
msgid "Incorrect lilypond version: %s (%s, %s)"
msgstr ""

#: lexer.ll:755 lexer.ll:756
msgid "Consider updating the input with the convert-ly script"
msgstr ""
