# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2003-07-24 18:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: fslint.c:7
msgid "FSlint"
msgstr ""

#: fslint.c:8 fslint.c:13
msgid "Add"
msgstr ""

#: fslint.c:9 fslint.c:14
msgid "Remove"
msgstr ""

#: fslint.c:10
msgid "Directories to search"
msgstr ""

#: fslint.c:11
msgid "recurse?"
msgstr ""

#: fslint.c:12
msgid "Search path"
msgstr ""

#: fslint.c:15
msgid "Paths to exclude"
msgstr ""

#: fslint.c:16
msgid "Extra find parameters"
msgstr ""

#: fslint.c:17
msgid "extra find filtering parameters"
msgstr ""

#: fslint.c:18
msgid "*"
msgstr ""

#: fslint.c:19
msgid "Advanced search parameters"
msgstr ""

#: fslint.c:20 fslint.c:25 fslint.c:37 fslint.c:45 fslint.c:57 fslint.c:61
#: fslint.c:68 fslint.c:73 fslint.c:78
msgid "Name"
msgstr ""

#: fslint.c:21 fslint.c:26 fslint.c:38 fslint.c:46 fslint.c:58 fslint.c:62
#: fslint.c:69 fslint.c:74 fslint.c:79
msgid "Directory"
msgstr ""

#: fslint.c:22 fslint.c:48 fslint.c:66 fslint.c:70 fslint.c:76 fslint.c:81
msgid "Date"
msgstr ""

#: fslint.c:23
msgid "Duplicates"
msgstr ""

#: fslint.c:24
msgid "Sensitivity="
msgstr ""

#: fslint.c:27
msgid "Bad names"
msgstr ""

#: fslint.c:28
msgid "Search ("
msgstr ""

#: fslint.c:29
msgid "$PATH"
msgstr ""

#: fslint.c:30
msgid ") for"
msgstr ""

#: fslint.c:31 ../FSlint:457
msgid "Conflicting files"
msgstr ""

#: fslint.c:32 fslint.c:36 ../FSlint:455 ../FSlint:464
msgid "Aliases"
msgstr ""

#: fslint.c:33 ../FSlint:470
msgid "Case conflicts"
msgstr ""

#: fslint.c:34 ../FSlint:466
msgid "Same names"
msgstr ""

#: fslint.c:35 ../FSlint:468
msgid "Same names(ignore case)"
msgstr ""

#: fslint.c:39 fslint.c:47 fslint.c:65 fslint.c:75 fslint.c:80
msgid "Size"
msgstr ""

#: fslint.c:40
msgid "Name clashes"
msgstr ""

#: fslint.c:41
msgid "More restrictive search for core files"
msgstr ""

#: fslint.c:42
msgid "core file mode?"
msgstr ""

#: fslint.c:43
msgid "days"
msgstr ""

#: fslint.c:44
msgid "minimum age"
msgstr ""

#: fslint.c:49
msgid "Temp files"
msgstr ""

#: fslint.c:50
msgid "Dangling"
msgstr ""

#: fslint.c:51
msgid "Absolute links to paths within or below the link's directory"
msgstr ""

#: fslint.c:52
msgid "Suspect"
msgstr ""

#: fslint.c:53
msgid "Relative"
msgstr ""

#: fslint.c:54
msgid "Absolute"
msgstr ""

#: fslint.c:55
msgid "/././. ///// /../ etc."
msgstr ""

#: fslint.c:56
msgid "Redundant naming"
msgstr ""

#: fslint.c:59
msgid "Target"
msgstr ""

#: fslint.c:60
msgid "Bad symlinks"
msgstr ""

#: fslint.c:63
msgid "UID"
msgstr ""

#: fslint.c:64
msgid "GID"
msgstr ""

#: fslint.c:67
msgid "Bad IDs"
msgstr ""

#: fslint.c:71
msgid "Empty directories"
msgstr ""

#: fslint.c:72
msgid "Search $PATH"
msgstr ""

#: fslint.c:77
msgid "Non stripped binaries"
msgstr ""

#: fslint.c:82
msgid "Redundant whitespace"
msgstr ""

#: fslint.c:83
msgid "Find"
msgstr ""

#: fslint.c:84
msgid "Stop"
msgstr ""

#: fslint.c:85
msgid "selection"
msgstr ""

#: fslint.c:86
msgid "Toggle"
msgstr ""

#: fslint.c:87
msgid "list to file"
msgstr ""

#: fslint.c:88
msgid "Save"
msgstr ""

#: fslint.c:89 fslint.c:93
msgid "selected"
msgstr ""

#: fslint.c:90
msgid "Delete"
msgstr ""

#: fslint.c:91
msgid "All (except selected) using hardlinks"
msgstr ""

#: fslint.c:92
msgid "Merge"
msgstr ""

#: fslint.c:94
msgid "Clean"
msgstr ""

#: fslint.c:95
msgid "errors"
msgstr ""

#: fslint.c:96
msgid "Select Path"
msgstr ""

#: ../FSlint:53
#, python-format
msgid "Usage: %s [OPTION] [PATHS]"
msgstr ""

#: ../FSlint:54
msgid "    --version       display version"
msgstr ""

#: ../FSlint:55
msgid "    --help          display help"
msgstr ""

#: ../FSlint:186
msgid "Do you want to overwrite?\n"
msgstr ""

#: ../FSlint:187 ../FSlint:726 ../FSlint:778 ../FSlint:820
msgid "Yes"
msgstr ""

#: ../FSlint:187 ../FSlint:726 ../FSlint:778 ../FSlint:820
msgid "No"
msgstr ""

#: ../FSlint:194
msgid "You can't overwrite "
msgstr ""

#: ../FSlint:237
msgid "User aborted"
msgstr ""

#: ../FSlint:261
msgid "No search paths specified"
msgstr ""

#: ../FSlint:346 ../FSlint:397 ../FSlint:444
msgid " files"
msgstr ""

#: ../FSlint:367
msgid " unstripped binaries"
msgstr ""

#: ../FSlint:378
msgid " empty directories"
msgstr ""

#: ../FSlint:419
msgid " links"
msgstr ""

#: ../FSlint:443 ../FSlint:568
msgid " bytes wasted in "
msgstr ""

#: ../FSlint:499 ../FSlint:569
msgid " files (in "
msgstr ""

#: ../FSlint:500 ../FSlint:570
msgid " groups)"
msgstr ""

#: ../FSlint:559
msgid "bytes wasted"
msgstr ""

#: ../FSlint:716
msgid "None selected"
msgstr ""

#: ../FSlint:722
msgid "Are you sure you want to delete this item?\n"
msgstr ""

#: ../FSlint:724
#, python-format
msgid "Are you sure you want to delete these %d"
msgstr ""

#: ../FSlint:725 ../FSlint:819
msgid " items?\n"
msgstr ""

#: ../FSlint:761
msgid " items deleted"
msgstr ""

#: ../FSlint:770
msgid "Are you sure you want to merge ALL files?\n"
msgstr ""

#: ../FSlint:774
msgid "(Ignoring those selected)\n"
msgstr ""

#: ../FSlint:818
msgid "Are you sure you want to clean these "
msgstr ""

#: ../FSlint:858
msgid " items cleaned ("
msgstr ""

#: ../FSlint:860
msgid " bytes saved)"
msgstr ""
